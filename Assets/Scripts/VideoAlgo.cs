using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.XR;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.Video;
using Unity.XR.Oculus;

using TensorFlowLite;


using UnityEngine.UI;

public class VideoAlgo : MonoBehaviour
{
    [SerializeField] public VideoPlayer vp;
    public Texture2D videoFrame;
    // Start is called before the first frame update
    void Start()
    {
        videoFrame = new Texture2D(2, 2);
        vp.sendFrameReadyEvents = true;
        vp.frameReady += OnNewFrame;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnNewFrame(VideoPlayer source, long frameIdx)
    {
        RenderTexture rend = source.texture as RenderTexture;
        if (vp.width != rend.width || vp.height != rend.height)
        {
            videoFrame.Resize(rend.width, rend.height);
        }
        RenderTexture.active = rend;
        videoFrame.ReadPixels(new Rect(0, 0, rend.width, rend.height), 0, 0);
        videoFrame.Apply();

        RenderTexture out_renderTexture = new RenderTexture(224, 224, 0);
        out_renderTexture.enableRandomWrite = true;
        out_renderTexture = rend;
        RenderTexture.active = null;
        Debug.Log("new frame");
        var r = GetComponent<Renderer>();
        r.material.mainTexture = rend;

    }

    Texture2D Resize(Texture2D texs, int targetX, int targetY)
    {
        RenderTexture rt = new RenderTexture(targetX, targetY, 24);
        RenderTexture.active = rt;
        Graphics.Blit(texs, rt);
        Texture2D result = new Texture2D(targetX, targetY);
        result.ReadPixels(new Rect(0, 0, targetX, targetY), 0, 0);
        result.Apply();
        return result;
    }
    Color32 CalculateAverageColorFromTexture(Texture2D tex)
    {
        Color32[] texColors = tex.GetPixels32();
        int total = texColors.Length;
        float r = 0;
        float g = 0;
        float b = 0;

        for (int i = 0; i < total; i++)
        {
            r += texColors[i].r;
            g += texColors[i].g;
            b += texColors[i].b;
        }

        return new Color32((byte)(r / total), (byte)(g / total), (byte)(b / total), 0);

    }
}
