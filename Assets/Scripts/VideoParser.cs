using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.XR;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.Video;
using Unity.XR.Oculus;

using TensorFlowLite;


using UnityEngine.UI;

public class VideoParser : MonoBehaviour
{
    [SerializeField] public VideoPlayer vp;
    public Texture2D videoFrame;
    [SerializeField, FilePopup("*.tflite")] string fileName = "mnist.tflite";
    [SerializeField] TextMesh outputTextView = null;
    [SerializeField] ComputeShader compute = null;

    Interpreter interpreter;
    bool isProcessing = false;
    float[,] inputs = new float[224, 224];
    float[] outputs = new float[7];
    ComputeBuffer inputBuffer;
    // Start is called before the first frame update
    void Start()
    {

        videoFrame = new Texture2D(2, 2);
        vp.sendFrameReadyEvents = true;
        vp.frameReady += OnNewFrame;
        var options = new InterpreterOptions()
        {
            threads = 2,
            useNNAPI = false,
        };
        interpreter = new Interpreter(FileUtil.LoadFile(fileName), options);
        interpreter.ResizeInputTensor(0, new int[] { 1, 224, 224, 3 });
        interpreter.AllocateTensors();
        inputBuffer = new ComputeBuffer(224 * 224 * 3, sizeof(float));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnNewFrame(VideoPlayer source, long frameIdx)
    {
        RenderTexture rend = source.texture as RenderTexture;
        if (vp.width != rend.width || vp.height != rend.height)
        {
            videoFrame.Resize(rend.width, rend.height);
        }
        RenderTexture.active = rend;
        videoFrame.ReadPixels(new Rect(0, 0, rend.width, rend.height), 0, 0);
        videoFrame.Apply();

        RenderTexture out_renderTexture = new RenderTexture(224, 224, 0);
        out_renderTexture.enableRandomWrite = true;
        out_renderTexture = rend;
        RenderTexture.active = null;
        Debug.Log("new frame");
        //var r = GetComponent<Renderer>();
        //r.material.mainTexture = rend;

    }

    Texture2D Resize(Texture2D texs, int targetX, int targetY)
    {
        RenderTexture rt = new RenderTexture(targetX, targetY, 24);
        RenderTexture.active = rt;
        Graphics.Blit(texs, rt);
        Texture2D result = new Texture2D(targetX, targetY);
        result.ReadPixels(new Rect(0, 0, targetX, targetY), 0, 0);
        result.Apply();
        return result;
    }
    Color32 CalculateAverageColorFromTexture(Texture2D tex)
    {
        Color32[] texColors = tex.GetPixels32();
        int total = texColors.Length;
        float r = 0;
        float g = 0;
        float b = 0;

        for (int i = 0; i < total; i++)
        {
            r += texColors[i].r;
            g += texColors[i].g;
            b += texColors[i].b;
        }

        return new Color32((byte)(r / total), (byte)(g / total), (byte)(b / total), 0);

    }
    void OnDestroy()
    {
        interpreter?.Dispose();
        inputBuffer?.Dispose();
    }

    public void OnDrawTexture(Texture2D texture)
    {
        if (!isProcessing)
        {
            Invoke(texture);
        }
    }

    void Invoke(Texture2D texture)
    {
        isProcessing = true;

        compute.SetTexture(0, "InputTexture", texture);
        compute.SetBuffer(0, "OutputTensor", inputBuffer);
        compute.Dispatch(0, 28 / 4, 28 / 4, 1);
        inputBuffer.GetData(inputs);

        float startTime = Time.realtimeSinceStartup;
        interpreter.SetInputTensorData(0, inputs);
        interpreter.Invoke();
        interpreter.GetOutputTensorData(0, outputs);
        float duration = Time.realtimeSinceStartup - startTime;

        isProcessing = false;
    }

    private static void ConveretTexture2dIntoRenderTexture(out RenderTexture out_renderTexture, in Texture2D input_texture2d, int resolution)
    {

        out_renderTexture = new RenderTexture(resolution, resolution, 0);
        out_renderTexture.enableRandomWrite = true;
        RenderTexture.active = out_renderTexture;
        // Copy your texture ref to the render texture
        Graphics.Blit(input_texture2d, out_renderTexture);



    }

}